import React from 'react';
import { Subscription } from "react-apollo";

import { messageSubs } from '../graphql/'

let variableSub = {
  message: {
    channelId: 1,
    text: 'test'
  }
}

const MessageSubscribe = () => (
  <Subscription
    subscription={messageSubs}
    variables={variableSub}
  >
    { ({ data: data, loading }) => {
      // data received here is only for new message.
      // ideally for notifications
      return (
        <p>New comment: {!loading && data.messageSub.text}</p>
      )
    } }
  </Subscription>
)

export default MessageSubscribe
