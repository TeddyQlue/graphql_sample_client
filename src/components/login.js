import React, { Component } from 'react';
import { graphql, compose } from 'react-apollo'
import gql from 'graphql-tag'

class Login extends Component {
  componentDidMount() {
    // console.log(this.props.UserStatusQuery);
    // this.getStatus()
  }

  componentDidUpdate() {
    console.log(this.props.UserStatusQuery.getUserStatus);
    console.log(this.props.loginUsername);
    if (this.props.UserStatusQuery.getUserStatus.status == true && this.props.loginUsername === '') {
      this.getStatus()
      console.log('in');
    }
  }

  getStatus = async () => {
    // let result = await this.props.loginMutation({
    //   variables: {
    //     username: this.props.UserStatusQuery.getUserStatus.username,
    //     password: 'password'
    //   }
    // })
    this.props.loggedIn(this.props.UserStatusQuery.getUserStatus.username)
  }

  handleSubmit = async (e) => {
    e.preventDefault()

    if (!this.props.username) {
      this.props.onError('username is required')
    }

    if (!this.props.password) {
      this.props.onError('password is required')
    }

    let username = this.props.username
    let password = this.props.password

    let result = await this.props.loginMutation({
      variables: {
        username: username,
        password: password
      }
    })
    this.props.loggedIn(username)
    this.props.login()

    const { token } = result.data.login
    console.log(token);
  }

  logout = async () => {
    let loginUsername = this.props.loginUsername
    let result = await this.props.logoutMutation({
      variables: {
        username: loginUsername
      }
    })
    this.props.loggedOut()
    // let { message } = result.data.logout
    console.log(result);
  }

  render() {
    if (this.props.UserStatusQuery && this.props.UserStatusQuery.loading) {
      return (
        <div>
          Loading...
        </div>
      )
    } else if (this.props.UserStatusQuery && this.props.UserStatusQuery.error) {
      return (
        <div>
          Error!
        </div>
      )
    } else if (this.props.UserStatusQuery.getUserStatus) {
      // console.log(this.props.UserStatusQuery.getUserStatus);
      return (
        <div className="Login">
          <form onSubmit={this.handleSubmit}>
            {
              this.props.error &&
              <h3 data-test="error" onClick={this.props.removeError}>
                <button onClick={this.props.removeError}>✖</button>
                {this.props.error}
              </h3>
            }
            <label>User Name</label>
            <input type="text" data-test="username" value={this.props.username} onChange={(e) => this.props.usernameChange(e)} />

            <label>Password</label>
            <input type="password" data-test="password" value={this.props.password} onChange={(e) => this.props.passwordChange(e)} />

            <input type="submit" value="Log In" data-test="submit" />
          </form>

          <button className="Logout" onClick={this.logout}>
            Logout
          </button>
        </div>
      )
    }

  }
}

const USERSTATUS_QUERY = gql`
  query UserStatusQuery {
    getUserStatus {
      status
      username
    }
  }
`

const LOGIN_MUTATION = gql`
  mutation LoginMutation($username: String!, $password: String!) {
    login(username: $username, password: $password) {
      token
    }
  }
`

const LOGOUT_MUTATION = gql`
  mutation LogoutMutation($username: String!) {
    logout(username: $username) {
      message
    }
  }
`

export default compose(
  graphql(LOGIN_MUTATION, { name: 'loginMutation' }),
  graphql(LOGOUT_MUTATION, { name: 'logoutMutation' }),
  graphql(USERSTATUS_QUERY, { name: 'UserStatusQuery' })
)(Login)
