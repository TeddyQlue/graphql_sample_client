import React, { Component } from 'react';
import { Mutation } from "react-apollo";

import { messageAdd, messageQuery } from '../graphql/'

export default class MessageMutate extends Component {
  constructor () {
    super()
    this.state = {
      text: '',
      channelId: '',
      messageList: []
    }
  }

  render () {
    return (
      <Mutation mutation={messageAdd}
        update={(cache, {data: {messageAdd} }) => {
          const cacheList = cache.readQuery({ query: messageQuery })

          // CHECK FOR DUPLICATE MESSAGE --- IDEALLY USE UNIQUE IDs
          let dupeCheck = cacheList.messageGet.some((el, index, arr) => {
            return el.text === messageAdd.text && el.channelId === messageAdd.channelId
          });

          if (!dupeCheck) {
            // Update Apollo's Local cache - [OPTIMISTIC UI]
            // will update the <Query> component with new data
            cacheList.messageGet.push(messageAdd)
            cache.writeQuery({
              query: messageQuery,
              data: cacheList
            })
          }

        }}
      >
        {(messageAdd, { data }) => (
          <div>
            <form
              onSubmit={e => {
                e.preventDefault();
                messageAdd({ variables: {
                  message: {
                    text: this.state.text,
                    channelId: 1
                  }
                }});
                this.setState({ text: '', channelId: 1});
              }}
            >
              <input
                type="text"
                value={this.state.text}
                onChange={(event) => this.setState({ text: event.target.value })}
              />
              <button type="submit">Add Todo</button>
            </form>
          </div>
        )}
      </Mutation>
    )
  }
}
