import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Login from './components/login.js'

/****************************
*    GRAPHQL COMPONENTS     *
****************************/
import MessageGet from './components/messageQuery.js'
import MessageMutate from './components/messageMutation.js'
import MessageSubscribe from './components/messageSubscription.js'

/******************************
*    APP SCREEN COMPONENT     *
******************************/
class App extends Component {
  constructor() {
    super()
    this.state = {
      loginUsername: '',
      username: '',
      password: '',
      error: '',
      loginStatus: false
    }
  }

  loggedIn = (username) => {
    this.setState({
      loginUsername: username,
      loginStatus: true
    })
  }

  loggedOut = (username) => {
    this.setState({
      loginUsername: '',
      loginStatus: false
    })
  }

  onError = (err) => {
    this.setState({
      error: err
    })
  }

  removeError = () => {
    this.setState({
      error: ''
    })
  }

  usernameChange = (e) => {
    this.setState({
      username: e.target.value
    })
  }

  passwordChange = (e) => {
    this.setState({
      password: e.target.value
    })
  }

  login =  () => {
    this.setState({
      username: '',
      password: ''
    })
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to React</h1>
        </header>
        <Login
          username={this.state.username}
          password={this.state.password}
          error={this.state.error}
          loginUsername={this.state.loginUsername}
          usernameChange={this.usernameChange}
          passwordChange={this.passwordChange}
          onError={this.onError}
          removeError={this.removeError}
          login={this.login}
          loggedIn={this.loggedIn}
          loggedOut={this.loggedOut}
        ></Login>
        {
          this.state.loginUsername.length > 0 &&
          <div>
            <MessageGet />
            <p className="App-intro">
              Start chatting!
            </p>
            <MessageMutate />
            <MessageSubscribe />
          </div>
        }
      </div>
    );
  }
}

export default App;
