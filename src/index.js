import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';

import { ApolloProvider } from 'react-apollo'
// import ApolloClient from 'apollo-boost'

import { ApolloClient } from 'apollo-client';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { HttpLink } from 'apollo-link-http';

import { WebSocketLink } from 'apollo-link-ws';
import { split } from 'apollo-link';
import { getMainDefinition } from 'apollo-utilities';

/****************************
*    APOLLO CACHE     *
****************************/
const cache = new InMemoryCache()

/***********************************
*    APOLLO BOOST CLIENT SETUP     *
***********************************/
// FOR QUICK IMPLEMENTATION
// NO ADVANCED FEATURES SUCH AS SUBSCRIPTION
// const client = new ApolloClient({
//   uri:'http://localhost:3000/graphql',
//   cache
// })

/********************************
*    APOLLO WEBSOCKET SETUP     *
********************************/
const local = 'http://localhost:3000'
const heroku = 'https://fast-beach-82436.herokuapp.com'
const herokuShort = 'fast-beach-82436.herokuapp.com'
// pass request over normal http link
const httpLink = new HttpLink({
  uri: `${heroku}/graphql`,
  credentials: 'include'
});

const prodWeb = 'graphql-subs-qfpvlpzbgq.now.sh'
const wsEndpointLocal = `ws://localhost:3000/subscriptions`
const wsEndpointHeroku = `wss://${herokuShort}/subscriptions`

// pass request over websocket link
const wsLink = new WebSocketLink({
  uri: wsEndpointHeroku,
  options: {
    reconnect: true
  }
});

/*
Split normal queries with httpLink
and websocket to wsLink
first argument is a filter
if true send traffic to argument no.2
if false send traffic to argument no.3

                ||=[true]=>[wsLink]=====>[graphql websocket endpoint]
[query] ==> [split]
                ||=[false]=>[httpLink]==>[graphql standard endpoint]

*/
const link = split(
  ({query}) => {
    const { kind, operation } = getMainDefinition(query)
    return kind === 'OperationDefinition' && operation === 'subscription';
  },
  wsLink,
  httpLink,
)

/**********************************
*    APOLLO LINK CLIENT SETUP     *
**********************************/
export const client = new ApolloClient({
  link: link,
  cache: cache
});


/**********************
*    REACT RENDER     *
**********************/
ReactDOM.render(
  <ApolloProvider client={client}>
    <App />
  </ApolloProvider>
  , document.getElementById('root'));
registerServiceWorker();
