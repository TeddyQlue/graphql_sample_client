import gql from "graphql-tag";

export const messageSubs = gql`
  subscription ($message: MessageInput!) {
    messageSub(message:$message){
      channelId
      text
    }
  }
`
