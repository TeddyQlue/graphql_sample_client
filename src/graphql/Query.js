import gql from "graphql-tag";

export const messageQuery = gql`
  query {
    messageGet {
      text
      channelId
      __typename
    }
  }
`
